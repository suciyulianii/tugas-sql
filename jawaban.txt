1. Membuat Database
create database my_shop;

2. Membuat Table di dalam Database
- users
create table users( id int auto_increment, name varchar(255), email varchar(255), password varchar(255), primary key(id));

- items
create table items( id int auto_increment, name varchar(255), description varchar(255), price int, stock int, category_id int, primary key(id), foreign key(category_id) references categories(id) );

- categories
create table categories( id int auto_increment, name varchar(255), primary key(id));

3. Memasukkan Data pada Table

- users
insert into users (name, email, password) VALUES ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@does.com", "jenita123");

- categories
insert into categories(name) VALUES ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

- items
insert into items (name, description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4. Mengambil Data dari Database
a. Mengambil data users
select id, name, email from users;

b. Mengambil data items
-select * from items where price > 1000000;

-select * from items where name like "%Watch";

c. Menampilkan data items join dengan kategori
select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items join categories on items.id=categories.id;

5. Mengubah Data dari Database
update items set price = 2500000 where name like"Sumsang%";
